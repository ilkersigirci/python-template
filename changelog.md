# Changelog

## 1.0.0 - 2022-12-25

### Added

-   Initial release

### Changed

### Deprecated

### Removed

### Fixed

### Security
